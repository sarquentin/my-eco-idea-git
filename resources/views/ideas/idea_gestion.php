<!DOCTYPE html>
<html lang="fr">
<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="theme-color" content="#35BF54">

	<link rel='shortcut icon' type='image/ico' href='/public/images/logo.png'>
	<link rel='logo' type='image/png' href='/public/images/logo.png'>


	<title>My EcoIdea</title>

	<meta http-equiv="content-language" content="fr">

  <link rel="stylesheet" type="text/css" href="/public/stylesheets/pages/idea_master.css">*
</head>
<body>
	<?php include($_SERVER['DOCUMENT_ROOT']."/public/apps/header.html") ?>
  <main class="disable">
    <div class="page-header">
      <h1>Gérer mes idées</h1>
    </div>
    <div class="disable popup"><!-- Popup delete idea -->
      <p>Voulez vous supprimer cette idée?</p>
        <input type="button" href="{idea_link_edit}" class="button edit" value="Modifier">
        <input type="button" class="button delete" value="Supprimer">
    </div>

    <div id="ideas-container"><!-- container Idées-->
    <div class="idea"><!-- Idée -->
      <div class="header"><!-- Head -->
        <img src="https://sarquentin.fr/src/img/logo.png" class="authorLogo" alt="{autorLogo}">
        <span class="authorName">Quentin Sar</span>
      </div>
      <div class="content"><!-- Preview -->
        <span>Remplacer les tickets de caisse papier par des ticket virtuel (email/sms)</span>
      </div>
      <div class="action"><!-- Div d'action -->
        <input type="button" href="{idea_link_edit}" class="button edit" value="Modifier">
        <input type="button" class="button delete" value="Supprimer">
      </div>
    </div>
  </div><!-- End container idées-->
  </main>
  <?php include($_SERVER['DOCUMENT_ROOT'].'/public/apps/footer.html');?>
  <style>
.disable { display: none; }
header { display: none; }
main { display: none; }
  </style>
  <script>
$(function() {
    $("header").fadeIn("slow");
    $("footer").fadeIn("slow");
    $("main").fadeIn("slow");

});

$('.delete')
  </script>
</body>
</html>
