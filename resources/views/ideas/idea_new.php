<!DOCTYPE html>
<html lang="fr">
<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">


	<link rel='shortcut icon' type='image/ico' href='/public/images/logo.png'>
	<link rel='logo' type='image/svg' href='/public/images/logo.png'>


	<title>My EcoIdea | Mon idée</title>

	<meta http-equiv="content-language" content="fr">

	<link rel="stylesheet" type="text/css" href="/public/stylesheets/pages/idea_new.css">
</head>
<body>
	<?php include("../../../public/apps/header.html") ?>
  <main>
    <div class="form">

      <form method="post">
        <h2>Partager son idée</h2>
        <p>3 mots clé qui représent l'idée (Lors d'une recherche ce sont eux qui permettent de la trouver)<br>
          <input name="keyword1" class="" required><input name="keyword2" required><input name="keyword3" required></p>
        <p>Preview de l'idée<br>
        <textarea name="preview" rows="2" required></textarea></p>
        <p>Description (N'hésitez pas à faire une rédaction si c'est un gros projet, plus c'est complet mieux c'est) <br>
        <textarea name="desc" style="resize: vertical;" rows="20" required></textarea></p>
        <input type="submit" value="Partager">
      </form>
  	</div>
  </main>
  <?php include("../../../public/apps/footer.html")?>
</body>
</html>
