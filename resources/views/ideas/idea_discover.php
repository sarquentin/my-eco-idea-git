<!DOCTYPE html>
<?php
$idea_content="{{idea_content}}";
$idea_id="{{idea_id}}";
?>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <title>My EcoIdea | Idée</title>

    <link rel='shortcut icon' type='image/ico' href='/public/images/logo.png'>
  	<link rel='logo' type='image/png' href='/public/images/logo.png'>

    <meta http-equiv="content-language" content="fr">

  	<link rel="stylesheet" type="text/css" href="/public/stylesheets/pages/idea_discover.css">

  </head>
  <body>
    <?php include("../../../public/apps/header.html") ?>
    <main>
      <div class="idea">
      <div class="user-info">
        <img src="https://via.placeholder.com/50">
        <p id="user_name">Quentin Sar</p>
        <p id="post_date">posté le 18/03/2019</p>
      </div>
        <div class="idea_header">
          <h3>Entreprise, Ticket de caisse, Papier</h3>
        </div>
        <div class="idea_content">
          <p><?php echo $idea_content; ?></p>
        </div>

        <div class="idea_info"><div id="heart" class="ic little fl"></div><i id="likes">15 J'aime</i><i id="adhesions">2 Adhésions</i></div>
      </div>
      <input id="adherer" class="xcenter" type="button" name="adherer" value="Adhérer">
      <div class="share">
        <h3>Partager cette idée</h3>
        <?php include('../../../public/apps/share.php'); ?>
      </div>

    </main>
    <script src="/public/js/icon.js">
    </script>
  </body>
</html>
