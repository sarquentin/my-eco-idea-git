<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <title>My EcoIdea | Idée</title>

    <link rel='shortcut icon' type='image/ico' href='/public/images/logo.png'>
  	<link rel='logo' type='image/png' href='/public/images/logo.png'>

    <meta http-equiv="content-language" content="fr">

  	<link rel="stylesheet" type="text/css" href="/public/stylesheets/idea_discover.css">

  </head>
  <body>
    <?php include("../../../public/apps/header.html") ?>
    <main>
      <div class="idea">
      <div class="user-info">
        <img src="https://via.placeholder.com/50">
        <p id="user_name">Quentin Sar</p>
        <p id="post_date">posté le 18/03/2019</p>
      </div>
        <div class="idea_header">
          <h3>Entreprise, Ticket de caisse, Papier</h3>
        </div>
        <div class="idea_content">
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam arcu nunc, suscipit pretium sapien scelerisque, euismod sagittis justo. Phasellus bibendum malesuada mauris ac commodo. Aenean vehicula eu neque ac tempor. Nunc efficitur maximus nibh sit amet mattis. Sed sit amet congue orci. Mauris eget eros eget odio porttitor tempus at non lacus. Pellentesque facilisis id justo a ultricies. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec suscipit urna ac finibus auctor. Ut a sodales massa, a ornare sem. Mauris nec egestas sem. Ut pellentesque in tellus sit amet convallis. Pellentesque tortor lectus, facilisis posuere sodales non, elementum et dolor.
In sed rhoncus diam. Vestibulum vestibulum accumsan pharetra. Integer enim purus, maximus et tempor sit amet, consequat eu tellus. Etiam fermentum metus eget velit bibendum venenatis. Nullam ultricies rhoncus faucibus. Praesent varius dignissim nunc, ac pretium erat laoreet quis. Cras convallis diam vitae diam luctus finibus. Nunc nec metus neque. Mauris non sem consectetur, condimentum erat sit amet, tincidunt ante. Fusce efficitur dignissim ligula, nec iaculis urna bibendum at. Duis consequat, purus et dictum fringilla, neque tellus accumsan dolor, ut vehicula felis ligula eget enim. Sed ac elit vitae mi tempor fermentum. In velit metus, elementum nec vehicula et, egestas non leo.</p>
        </div>
        <p><i id="likes">15 J'aime</i><i id="adhesions">2 Adhésions</i></p>
      </div>
      <input id="adherer" type="button" name="adherer" value="Adhérer">
    </main>
  </body>
</html>
