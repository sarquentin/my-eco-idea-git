<!DOCTYPE html>
<html lang="fr">
<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="theme-color" content="#35BF54">

  <link rel='shortcut icon' type='image/ico' href='/public/images/logo.png'>
	<link rel='logo' type='image/png' href='/public/images/logo.png'>


	<title>My EcoIdea</title>

	<meta http-equiv="content-language" content="fr">

  <link rel="stylesheet" type="text/css" href="/public/stylesheets/pages/idea_master.css">*
</head>
<body>
	<?php include($_SERVER['DOCUMENT_ROOT']."/public/apps/header.html") ?>
  <main class="disable">
    <div class="page-header">
      <h1>Les idées que j'adhère</h1>
    </div>

    <div id="ideas-container"><!-- container Idées-->
    <div class="idea"><!-- Idée -->
      <div class="header"><!-- Head -->
        <img src="https://sarquentin.fr/src/img/logo.png" class="authorLogo" alt="{autorLogo}">
        <span class="authorName">Quentin Sar</span>
      </div>
      <div class="content"><!-- Preview -->
        <span>Remplacer les tickets de caisse papier par des ticket virtuel (email/sms)</span>
      </div>
      <div class="action"><!-- Div d'action -->
        <input type="button" href="{idea_link}" class="button discover" value="Découvrir">
        <div id="heart" class="ic medium fl"></div><p>3909</p>
      </div>
    </div>
    <div class="idea"><!-- idée-->
      <div class="header">
        <img src="/public/images/iqhawe.jpg" class="authorLogo" alt="{autorLogo}">
        <span class="authorName">Iqhawe</span>
      </div>
      <div class="content">
        <span>Amener ses propres poches en papier au supermarché, boycotter les emballages.</span>
      </div>
      <div class="action">
        <input type="button" href="{idea_link}" class="button discover" value="Découvrir">
        <div id="heart" class="ic medium fl"></div><p>{idea_like}</p>
      </div>
    </div><!-- End idée -->
    <div class="idea"><!-- idée-->
      <div class="header">
        <img src="https://via.placeholder.com/50" class="authorLogo" alt="{autorLogo}">
        <span class="authorName">Spileur</span>
      </div>
      <div class="content">
        <span>Ton idée?</span>
      </div>
      <div class="action">
        <input type="button" href="{idea_link}" class="button discover" value="Découvrir">
        <div id="heart" class="ic medium fl"></div><p>{idea_like}</p>
      </div>
    </div><!-- End idée -->
    <div class="idea"><!-- idée-->
      <div class="header">
        <img src="https://via.placeholder.com/50" class="authorLogo" alt="{autorLogo}">
        <span class="authorName">Célia</span>
      </div>
      <div class="content">
        <span>Pour les distributeurs, amener ses propres bouteilles/gourdes à remplir</span>
      </div>
      <div class="action">
        <input type="button" href="{idea_link}" class="button discover" value="Découvrir">
        <div id="heart" class="ic medium fl"></div><p>120</p>
      </div>
    </div><!-- End idée -->
    <div class="idea load_more">
      <button name="load_more" id="load_more_button">
        <i class="fas fa-sync-alt"></i>
      </button>
    </div>
  </div><!-- End container idées-->
  </main>
  <?php include($_SERVER['DOCUMENT_ROOT'].'/public/apps/footer.html');?>
  <style>
.disable { display: none; }
header { display: none; }
main { display: none; }
  </style>
  <script>
$(function() {
    $("header").fadeIn("slow");
    $("footer").fadeIn("slow");
    $("main").fadeIn("slow");

});
  </script>
</body>
</html>
