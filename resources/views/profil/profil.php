<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <title>Profil</title>

    <link rel='shortcut icon' type='image/ico' href='/public/images/logo.png'>
  	<link rel='logo' type='image/png' href='/public/images/logo.png'>

    <meta http-equiv="content-language" content="fr">

    <link rel="stylesheet" type="text/css" href="/public/stylesheets/pages/profil_styles.css">
  </head>
  <body>
    <?php include("../../../public/apps/header.html") ?>
    <div class="main">
      <div class="feed" method="get" action="{{ route('modify_profile') }}">

        <img height="150" alt="test Avatar" src="../../../public/images/logo.png">
        <h2>{user_name}</h2>
        <span>Inscrit le {date_inscription}</span>
        <p>
          <span>Idée(s) posté(s) : {nb_idea}</span>
        </p>
        <div class="action">
        <a href="/resources/views/profil/profil_edition.php"><span>Modifier le profil</span></a>
        <p>
          <a><input class="light" type="button" value="Mes idées"></a>
          <a><input class="light" type="button" value="Idées likés"></a>
          <a><input class="light" type="button" value="Idées que j'adhère"></a>
        </p>
      </div>
      </div>
    </div>
  </body>
</html>
