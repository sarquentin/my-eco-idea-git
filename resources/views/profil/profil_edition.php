<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <title>Edition du profil</title>

    <link rel='shortcut icon' type='image/ico' href='/public/images/logo.png'>
  	<link rel='logo' type='image/png' href='/public/images/logo.png'>

    <meta http-equiv="content-language" content="fr">

    <link rel="stylesheet" type="text/css" href="/public/stylesheets/pages/profil_styles.css">
  </head>
  <body>
    <?php include("../../../public/apps/header.html") ?>
    <div class="main">
      <form class="feed" method="get" action="{{ route('modify_profile') }}">
        <h2>Modifier son profil</h2>
        <img height="150" alt="test Avatar" src="../../../public/images/logo.png"><br>
        <label for="avatarFile">Changer d'avatar<input name="avatar" class="disable" id="avatarFile" aria-describedby="fileHelp" type="file" accept="image/png, image/jpeg"></label>
        <p>
        <input class="flow" name="pseudo" id="pseudo" value="{{ old('pseudo') }}" placeholder="Pseudo" required>
        <input class="flow" name="email" id="email" type="email" value="{{ old('email') }}" placeholder="Email" required>
        </p>
        <div id="reset-password">
          <input class="flow button" type="button" value="Modifier le mot de passe" required>
          <input class="flow" name="password" id="password" type="password" placeholder="Mot de passe" value="{{ old('password') }}" required>
          <input class="flow" type="password" placeholder="Confirmer le mot de passe" value="{{ old('password') }}" required>
        </div>
        <input class="flow" id="save" type="submit" value="Enregistrer">
      </form>
    </div>
  </body>
</html>
