<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <title>Inscription</title>

    <link rel='shortcut icon' type='image/ico' href='/public/images/logo.png'>
  	<link rel='logo' type='image/png' href='/public/images/logo.png'>

    <meta http-equiv="content-language" content="fr">

  	<link rel="stylesheet" type="text/css" href="/public/stylesheets/pages/auth.css">

  </head>
  <body>
    <?php include("../../../public/apps/header.html") ?>
    <main>
      <div class="register main">
        <form class="feed" method="POST" action="{{ route('register') }}">
          <h2>S'inscrire</h2>
          <input class="flow" name="pseudo" id="pseudo" placeholder="Pseudo">
          <input class="flow" name="email" id="email" type="email" placeholder="Email">
          <input class="flow" name="password" type="password" placeholder="Mot de passe">
          <input class="flow" type="password" placeholder="Confirmer le mot de passe">
          <span class="err disable" id="Pwd">Les mots de passe ne correspondent pas</span>
          <span class="err disable" id="Champ">Tout les champs ne sont pas remplis</span>
          <span class="err disable" id="Cgu">Veuillez accepter les CGU pour vous inscrire</span>
          <label for="cguConfirm">J'accepte les <a href="/cgu/">CGU</a><input type="checkbox" id="cguConfirm"></label>
          <input class="flow" id="submit" type="submit" value="S'inscrire">
          <span><a href="{{ route('login') }}">Se connecter</a></span>
        </form>
      </div>
    </main>
  <script type="text/javascript">
$(function() {
  $("header").fadeIn("slow");
});

  $(".flow").blur(function() {
    if ($(this).val() == "") {
      $(this).removeClass("valid");
      $(this).addClass("empty");
      var placeholder = $(this).attr("placeholder");
      if (!placeholder.includes(" (Obligatoire)")) {
        placeholder += " (Obligatoire)";
        $(this).attr("placeholder", placeholder);
      }
    } else {
      $(this).removeClass("empty");
      $(this).addClass("valid");
    }
  });

  $("input[type='password']").change(function() {
    if ($("input[type='password']:first").val() != $("input[type='password']:last").val()) {
      $(".err#Pwd").removeClass("disable");
    } else {
      $(".err#Pwd").addClass("disable");
    }
  });

  $("#submit").click(function() {
    //Check si tt les champs sont remplis
    if (($(".empty").length == 0) && ($(".valid").length != 0)) {
      //enleve l'error des champs
      $(".err#Champ").addClass("disable");

      //check si les mdp sont pareils
      if ($(".err#Pwd").hasClass("disable")) {

        //check si les cgu sont acceptés
        if ($("#cguConfirm").is( ":checked" )) {
          $("form.feed").submit();
        } else {

          $(".err#Cgu").removeClass("disable");
          return(false);
        }
      } else {
        return(false);
      }
    } else {

      $(".err#Champ").removeClass("disable");
      return(false);
    }
  });
  </script>
  </body>
</html>
