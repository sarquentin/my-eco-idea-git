<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <title>Connexion</title>

    <link rel='shortcut icon' type='image/ico' href='/public/images/logo.png'>
  	<link rel='logo' type='image/png' href='/public/images/logo.png'>

    <meta http-equiv="content-language" content="fr">

    <link rel="stylesheet" type="text/css" href="/public/stylesheets/pages/auth.css">
    <link rel="stylesheet" type="text/css" href="/public/stylesheets/master.css">
  </head>
  <body>
    <?php include("../../../public/apps/header.html") ?>
    <main>
      <div class="login main">
        <form class="feed" method="get" action="{{ route('login') }}">
          <h2>Se connecter</h2>
          <input class="flow" name="email" id="email" type="email" value="{{ old('email') }}" placeholder="Email">
          <input class="flow" name="password" id="password" type="password" placeholder="Mot de passe">
          <label for="remember">Se souvenir de moi<input name="remember" type="checkbox" id="remember" {{ old('remember') ? 'checked' : '' }}></label>
          <input class="flow" id="submit" type="submit" value="Se connecter">
          <span><a href="{{ route('register') }}">S'inscrire</a></span>
        </form>
      </div>
    </main>
  <script type="text/javascript">
  $(function() {
    $("header").fadeIn("slow");
  });

    $(".flow").blur(function() {
      if ($(this).val() == "") {
        $(this).removeClass("valid");
        $(this).addClass("empty");
        var placeholder = $(this).attr("placeholder");
        if (!placeholder.includes(" (Obligatoire)")) {
          placeholder += " (Obligatoire)";
          $(this).attr("placeholder", placeholder);
        }
      } else {
        $(this).removeClass("empty");
        $(this).addClass("valid");
      }
    });

    $("#submit").click(function() {
      if (($(".empty").length = 0) || (($("#email").val()!="") && ($("#password").val()!=""))) {
        $("form.feed").submit();
      } else {
        return(false);
      }
    });
  </script>
  </body>
</html>
