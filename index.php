<!DOCTYPE html>
<html lang="fr">
<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

	<meta name="keywords" content="quentin, sar">
	<meta name="description" content="Dans une démarche éco-citoyenne, nous aimerions partager vos idées afin d'améliorer le quotidien des gens mais également changer les habitudes des plus grosse entreprises.">
	<meta name="author" content="Quentin Sar, sarquentin.fr, Spileur, Iqhwe">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="reply-to" content="contact@sarquentin.fr">
	<meta name='subject' content="subject_empty">
	<meta name='language' content='FR'>
	<meta name='owner' content='Quentin Sar'>
	<meta name='url' content='https://my-ecoidea.org'>
	<meta name='identifier-URL' content='https://my-ecoidea.org'>
	<meta name='target' content='all'>
	<meta name="theme-color" content="#35BF54">

	<link rel='shortcut icon' type='image/ico' href='public/images/logo.png'>
	<link rel='logo' type='image/png' href='public/images/logo.png'>

	<meta property="og:title" content="My EcoIdea" />
	<meta property="og:description" content="Dans une démarche éco-citoyenne, nous aimerions partager vos idées afin d'améliorer le quotidien des gens mais également changer les habitudes des plus grosse entreprises." />
	<meta property="og:image" content="https://my-ecoidea.sarquentin.fr/public/images/logo.png" />
	<meta property="og:site_name" content="My EcoIdea" />
	<meta property="og:type" content="website" />
	<meta property="og:locale" content="fr_FR" />

	<meta name="twitter:card" content="summary" />
	<meta name="twitter:site" content="@K_Dev_" />
	<meta name="twitter:title" content="My EcoIdea" />
	<meta name="twitter:description" content="Dans une démarche éco-citoyenne, nous aimerions partager vos idées afin d'améliorer le quotidien des gens mais également changer les habitudes des plus grosse entreprises." />
	<meta name="twitter:image" content="public/images/logo.png" />

	<title>My EcoIdea</title>

	<meta http-equiv="content-language" content="fr">

  <link rel="stylesheet" type="text/css" href="/public/stylesheets/pages/idea_master.css">
  <link rel="stylesheet" type="text/css" href="public/stylesheets/index.css">
  <script language=javascript>
var isRedirect = false;
var redirectagent = navigator.userAgent.toLowerCase();
var redirect_devices = ['vnd.wap.xhtml+xml', 'sony', 'symbian', 'nokia', 'samsung', 'mobile', 'windows ce', 'epoc', 'opera mini', 'nitro', 'j2me', 'midp-', 'cldc-', 'netfront', 'mot', 'up.browser', 'up.link', 'audiovox', 'blackberry', 'ericsson', 'panasonic', 'philips', 'sanyo', 'sharp', 'sie-', 'portalmmm', 'blazer', 'avantgo', 'danger', 'palm', 'series60', 'palmsource', 'pocketpc', 'smartphone', 'rover', 'ipaq', 'au-mic', 'alcatel', 'ericy', 'vodafone', 'wap1', 'wap2', 'teleca', 'playstation', 'lge', 'lg-', 'iphone', 'android', 'htc', 'dream', 'webos', 'bolt', 'nintendo'];
for (var i in redirect_devices) {
  if (redirectagent.indexOf(redirect_devices[i]) != -1) {
    window.location = "https://mobile.my-ecoidea.org";
    isRedirect = true;
  }
}
</script>
</head>
<body>

  <div class="load">
    <div id="load_circle"></div>
    <div id="load_circle_2"></div>
    <div id="load_circle_reverse"></div>
  </div>
	<?php include("public/apps/header.html") ?>
  <!--<?php include("public/apps/popup_choose_platform.php") ?>-->
  <main class="disable">
    <div class="page-header">
      <h1>Découvrez des idées</h1>
      <div class="search">
        <p id="search_select">Trier par</p>
        <form id="search_selector">
          <select name="nom" size="1">
            <option>Nouveautés</option>
            <option>Les plus récentes</option>
            <option>Des + aimés aux - aimés</option>
            <option>Des - aimés aux + aimés</option>
            <option>Des + adhérés aux - adhérés</option>
            <option>Des - adhérés aux + adhérés</option>
            <option>Des + visités aux - visités</option>
            <option>Des - visités aux + visités</option>
          </select>
        </form>
      </div>
    </div>

    <div id="ideas-container"><!-- container Idées-->
    <div class="idea"><!-- Idée -->
      <div class="header"><!-- Head -->
        <img src="https://sarquentin.fr/src/img/logo.png" class="authorLogo" alt="{autorLogo}">
        <span class="authorName">Quentin Sar</span>
      </div>
      <div class="content"><!-- Preview -->
        <span>Remplacer les tickets de caisse papier par des ticket virtuel (email/sms)</span>
      </div>
      <div class="action"><!-- Div d'action -->
        <input type="button" href="{idea_link}" class="button discover" value="Découvrir">
        <div id="heart" class="ic medium fl"></div><p>3909</p>
      </div>
    </div>
    <div class="idea"><!-- idée-->
      <div class="header">
        <img src="public/images/iqhawe.jpg" class="authorLogo" alt="{autorLogo}">
        <span class="authorName">Iqhawe</span>
      </div>
      <div class="content">
        <span>Amener ses propres poches en papier au supermarché, boycotter les emballages.</span>
      </div>
      <div class="action">
        <input type="button" href="{idea_link}" class="button discover" value="Découvrir">
        <div id="heart" class="ic medium fl"></div><p>{idea_like}</p>
      </div>
    </div><!-- End idée -->
    <div class="idea"><!-- idée-->
      <div class="header">
        <img src="https://via.placeholder.com/50" class="authorLogo" alt="{autorLogo}">
        <span class="authorName">Spileur</span>
      </div>
      <div class="content">
        <span>Ton idée?</span>
      </div>
      <div class="action">
        <input type="button" href="{idea_link}" class="button discover" value="Découvrir">
        <div id="heart" class="ic medium fl"></div><p>{idea_like}</p>
      </div>
    </div><!-- End idée -->
    <div class="idea"><!-- idée-->
      <div class="header">
        <img src="https://via.placeholder.com/50" class="authorLogo" alt="{autorLogo}">
        <span class="authorName">Célia</span>
      </div>
      <div class="content">
        <span>Pour les distributeurs, amener ses propres bouteilles/gourdes à remplir</span>
      </div>
      <div class="action">
        <input type="button" href="{idea_link}" class="button discover" value="Découvrir">
        <div id="heart" class="ic medium fl"></div><p>120</p>
      </div>
    </div><!-- End idée -->
    <div class="idea load_more">
      <button name="load_more" id="load_more_button">
        <i class="fas fa-sync-alt"></i>
      </button>
    </div>
  </div><!-- End container idées-->
  </main>
  <?php include 'public/apps/footer.html';?>
  <script src="public/js/icon.js"></script>
  <style>
.disable { display: none; }
header { display: none; }
main { display: none; }
  </style>
  <script src="public/js/icon.js">
  </script>
  <script>
$(function() {
$(".load").fadeOut("slow", function() {
    $("header").fadeIn("slow");
    $("footer").fadeIn("slow");
    $("main").fadeIn("slow");
  });
});
  </script>
</body>
</html>
