﻿<!DOCTYPE html>
<html>
<head>
	<title>Changement de version</title>
</head>
<body>
	<main>
		<h1>Voulez vous passer en version mobile ?</h1>
		<h3>Nous avons détecté que vous aviez un mobile, hors la version pc peut causer des problèmes de compatibilité avec votre appareil.</h3>
		<div class="choose">
			<a id="pc" href="/accueil.php">Rester sur la version pc</a>
			<a id="mobile" href="http://mobile.<?php echo($_SERVER['SERVER_NAME']); ?>/">Version mobile</a>
		</div>
	</main>
</body>
</html>

<style type="text/css">
.choose #pc
{
	color: var(--green);
	padding: 10px;
	line-height: 3em;
	margin-right: 10px;
  font-size: 2em;
}
.choose #mobile
{
	margin-left: 10px;
	color: var(--white);
	padding: 10px;
	font-size: 2.5em;
	background-color: var(--green);
	box-shadow: var(--shdw-df);
	border-radius: 5px;
}
.choose #mobile:hover { box-shadow: var(--shdw-df-hov); }
h1 { font-size: 5em; }
h3
{
  font-size: 3.5em;
	margin-top: 25px;
	margin-bottom: 50px;
}


/*
########################
#    My-EcoIdea.org    #
#      master.css      #
########################

Bienvenue petit curieux !
Tu comprends quelque chose la-dedans ?
En tout cas, on cherche des gens comme toi !

Contacte-nous sur twitter/instagram @my_ecoidea ou discord https://discord.gg/uBG8ErA
*/

:root
{
  /* Declaration des couleurs */
  --green : #35BF54;
  --green-02 : #237F38;
  --gray : #707070;
  --white : #ffffff;
  --blue: #63a4ff;
  --blue-01 : #36a5f9;

  /* Declaration des ombres */
  --shdw-btn-li: #bbb 0px 3px 10px ;  /*Bouton light*/
  --shdw-btn: #999 0px 3px 10px ;     /*Bouton*/
  --shdw-btn-hov: #777 0px 4px 10px;  /*Bouton hover*/
  --shdw-df: #eee 0px 5px 10px;       /*Defaut*/
  --shdw-df-hov: #ccc 0px 5px 10px;   /*Defaut hover*/
  --shdw-01: #aaa 0px 0px 10px;       /*Inconnu au bataillon*/
  --shdw-idea: 0px 1px 10px #ccc;     /* Idea form*/
  --shdw-idea-hov: 0px 1px 14px #777; /* Idea form hover */

  /* Declaration des bordures */
  --border-01: #ccc 1px solid;
  --border-01f: #666 1px solid;
  --border-01-blue: var(--blue) 1px solid;
  --border-02-blue: var(--blue) 2px solid;

  /* Couleurs par defaut */
  color : var(--gray);
  background-color : var(--white);

  /* Remise à zero de toutes les positions */
  padding: 0;
  margin: 0;
  outline: 0;
  border: none;

  /* Selection de la police d'ecriture */
  font-family: Arial;
  /* Ajustement de la taille des box */
  box-sizing: border-box;

  overflow-x: hidden;
}

body
{
  min-width: 100%;
  margin: 0;
}

* {
  transition: .5s;
}

a, a:hover, a:active, a:visited
{
  text-decoration: none;
  cursor: pointer;
}
main
{
	position: absolute;
	top: 50%;
	transform: translateY(-50%);

  padding-left: 5vw;
  padding-right: 5vw;

  width: 90vw;
  text-align: center;
}
h1
{
  width: 100%;

  color: var(--gray);
  border-bottom: 5px solid var(--green);
}
</style>
